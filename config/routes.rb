Rails.application.routes.draw do
  resources :posts
  root "users#index"

  resources :users
end
